package problPentalog;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class ProductMax {
	private static int n;//dimensiunea matricei
	private static int m;//numarul de factori ai produsului

	private static int[][] matrix;

	public static void main(String[] args) {
		ScanProperties();
		SetupMatrix();
		InitMatrix();
		System.out.println(MaxProduct());
	}

	public static void SetupMatrix() {
		matrix = new int[n][n];
	}

	private static int MaxProduct() {
		int maxLine = MaxLineProduct();
		int maxColumns = MaxColumnProduct();
		int maxDiagonalDown = MaxDiagonalProductDown();
		int maxDiagonalUp = MaxDiagonalProductUp();
		int maxSecDiagonal=MaxSecDiagonalProduct();
		int[] macszs = { maxLine, maxColumns, maxDiagonalDown, maxDiagonalUp, maxSecDiagonal};
		Arrays.sort(macszs);
		return macszs[4];
	}

	public static void InitMatrix() {
		if (matrix.length > 0) {
			Random rand = new Random();
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					matrix[i][j] = Math.abs(rand.nextInt()) % 10;
					System.out.print(matrix[i][j] + " ");
				}
				System.out.println();
			}
		}
	}

	private static int MaxLineProduct() {
		int maxProduct = 0;
		int tempMaxProduct = 1;

		for (int i = 0; i < n; i++) {
			int x = 0;
			while (x + m <= n) {
				tempMaxProduct = 1;
				for (int k = x; k < x + m; k++) {
					tempMaxProduct *= matrix[i][k];
				}

				if (maxProduct < tempMaxProduct) {
					maxProduct = tempMaxProduct;
				}
				x++;
			}
		}
		return maxProduct;
	}

	private static int MaxColumnProduct() {
		int maxProduct = 0;
		int tempMaxProduct = 1;

		for (int j = 0; j < n; j++) {
			int x = 0;
			while (x + m <= n) {
				tempMaxProduct = 1;
				for (int k = x; k < x + m; k++) {
					tempMaxProduct *= matrix[k][j];
				}

				if (maxProduct < tempMaxProduct) {
					maxProduct = tempMaxProduct;
				}
				x++;
			}
		}
		return maxProduct;
	}

	private static int MaxDiagonalProductDown() {
		int maxProduct = 0;
		int tempMaxProduct = 1;

		for (int i = 0; i <= n - m; i++) {

			int myJ = 0;
			int myI = i;
			while (myI + m <= n) {
				tempMaxProduct = 1;
				for (int k = 0; k < m; ++k) {
					tempMaxProduct *= matrix[myI + k][myJ + k];
				}
				if (maxProduct < tempMaxProduct) {
					maxProduct = tempMaxProduct;
				}
				myJ++;
				myI++;
			}
		}

		return maxProduct;
	}

	private static int MaxDiagonalProductUp() {
		int maxProduct = 0;
		int tempMaxProduct = 1;

		// pui j =1 daca vrei sa testezi si pe diagonale. (doar test).
		for (int j = 1; j <= n - m; j++) {

			int myJ = j;
			int myI = 0;
			while (myJ + m <= n) {
				tempMaxProduct = 1;
				for (int k = 0; k < m; ++k) {
					tempMaxProduct *= matrix[myI + k][myJ + k];
				}
				if (maxProduct < tempMaxProduct) {
					maxProduct = tempMaxProduct;
				}
				myJ++;
				myI++;
			}
		}

		return maxProduct;
	}

	public static int MaxSecDiagonalProduct()
	{
		//calculez matricea transpusa
		for(int i = 0; i < n; ++i)
			for(int j = 0; j < n; ++j)
			{
				int aux=matrix[i][j];
				matrix[i][j]=matrix[j][i];
				matrix[j][i]=aux;
			}
		int max1=MaxDiagonalProductUp();
		int max2=MaxDiagonalProductDown();
		if(max1>=max2)
			return max1;
		else
			return max2;
	}
	public static void ScanProperties() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("n = ");
		n = scanner.nextInt();
		System.out.println();
		System.out.print("m = ");
		m = scanner.nextInt();
		scanner.close();
		scanner = null;
	}
}
